<?php
session_start();
error_reporting(0);
include('includes/config.php');
if (strlen($_SESSION['admin_id']) == 0) {
    header('location:admin_page.php');
} else {

    if (isset($_POST['update'])) {
        $book_name = $_POST['book_name'];
        $pages = $_POST['pages'];
        $author_name = $_POST['author_name'];
        $category_name = $_POST['category_name'];
       
        $bookid = intval($_GET['bookid']);

        $sql = "update  book set book_name=:book_name,CatId=:category_name,AuthorId=:author_name,pages=:pages where id=:bookid";
        $query = $conn->prepare($sql);
        $query->bindParam(':book_name', $book_name, PDO::PARAM_STR);
        $query->bindParam(':pages', $pages, PDO::PARAM_STR);
        $query->bindParam(':author_name', $author_name, PDO::PARAM_STR);
        $query->bindParam(':category_name', $category_name, PDO::PARAM_STR);
        $query->bindParam(':bookid', $bookid, PDO::PARAM_STR);
        $query->execute();
        echo "<script>alert('Book info updated successfully');</script>";
        echo "<script>window.location.href='manage-books.php'</script>";
    }
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Online Library Management System | Edit Book</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="../proekt2/admin/assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONT AWESOME STYLE  -->
        <link href="../proekt2/admin/assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE  -->
        <link href="../proekt2/admin/assets/css/style.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    </head>

    <body>
        <!------MENU SECTION START-->
        <?php include('includes/header.php'); ?>
        <!-- MENU SECTION END-->
        <div class="content-wrapper">
            <div class="container">
                <div class="row pad-botm">
                    <div class="col-md-12">
                        <h4 class="header-line">Add Book</h4>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md12 col-sm-12 col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Book Info
                            </div>
                            <div class="panel-body">
                                <form role="form" method="post">
                                    <?php
                                    $bookid = intval($_GET['bookid']);
                                    $sql = "SELECT book.book_name,category.category_name,category.id as cid,author.author_name,author.id as athrid,book.pages,book.id as bookid,book.bookImage from  book join category on category.id=book.CatId join author on author.id=book.AuthorId where book.id=:bookid";
                                    $query = $conn->prepare($sql);
                                    $query->bindParam(':bookid', $bookid, PDO::PARAM_STR);
                                    $query->execute();
                                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt = 1;
                                    if ($query->rowCount() > 0) {
                                        foreach ($results as $result) {               ?>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Book Image</label>
                                                    <img src="bookimg/<?php echo htmlentities($result->bookImage); ?>" width="100">
                                                    <a href="change-bookimg.php?bookid=<?php echo htmlentities($result->bookid); ?>">Change Book Image</a>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Book Name<span style="color:red;">*</span></label>
                                                    <input class="form-control" type="text" name="book_name" value="<?php echo htmlentities($result->book_name); ?>" required />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> Category<span style="color:red;">*</span></label>
                                                    <select class="form-control" name="category_name" required="required">
                                                        <option value="<?php echo htmlentities($result->cid); ?>"> <?php echo htmlentities($category_name = $result->category_name); ?></option>
                                                        <?php

                                                        $sql1 = "SELECT * from  category ";
                                                        $query1 = $conn->prepare($sql1);

                                                        $query1->execute();
                                                        $resultss = $query1->fetchAll(PDO::FETCH_OBJ);
                                                        if ($query1->rowCount() > 0) {
                                                            foreach ($resultss as $row) {
                                                                if ($category_name == $row->category_name) {
                                                                    continue;
                                                                } else {
                                                        ?>
                                                                    <option value="<?php echo htmlentities($row->id); ?>"><?php echo htmlentities($row->category_name); ?></option>
                                                        <?php }
                                                            }
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> Author<span style="color:red;">*</span></label>
                                                    <select class="form-control" name="author_name" required="required">
                                                        <option value="<?php echo htmlentities($result->athrid); ?>"> <?php echo htmlentities($author_name = $result->author_name); ?></option>
                                                        <?php

                                                        $sql2 = "SELECT * from author ";
                                                        $query2 = $conn->prepare($sql2);
                                                        $query2->execute();
                                                        $result2 = $query2->fetchAll(PDO::FETCH_OBJ);
                                                        if ($query2->rowCount() > 0) {
                                                            foreach ($result2 as $ret) {
                                                                if ($author_name == $ret->author_name) {
                                                                    continue;
                                                                } else {

                                                        ?>
                                                                    <option value="<?php echo htmlentities($ret->id); ?>"><?php echo htmlentities($ret->author_name); ?></option>
                                                        <?php }
                                                            }
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Pages<span style="color:red;">*</span></label>
                                                    <input class="form-control" type="text" name="pages" value="<?php echo htmlentities($result->pages); ?>" required="required" />
                                                </div>
                                            </div>
                                    <?php }
                                    } ?><div class="col-md-12">
                                        <button type="submit" name="update" class="btn btn-info">Update </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <?php include('includes/footer.php'); ?>
        <!-- FOOTER SECTION END-->
        <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY  -->
        <script src="../proekt2/admin/assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="../proekt2/admin/assets/js/bootstrap.js"></script>
        <!-- CUSTOM SCRIPTS  -->
        <script src="../proekt2/admin/assets/js/custom.js"></script>
    </body>

    </html>
<?php } ?>