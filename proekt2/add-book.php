<?php
session_start();
error_reporting(0);
include 'config.php';
if (strlen($_SESSION['admin_id']) == 0) {
    header('location:admin_page.php');
} else {

    if (isset($_POST['add'])) {
        $book_name = $_POST['book_name'];

        $category_name = $_POST['category_name'];
        $author_name = $_POST['author_name'];

        $bookimg = $_FILES["bookpic"]["name"];
        // get the image extension
        $extension = substr($bookimg, strlen($bookimg) - 4, strlen($bookimg));
        // allowed extensions
        $allowed_extensions = array(".jpg", "jpeg", ".png", ".gif");
        // Validation for allowed extensions .in_array() function searches an array for a specific value.
        //rename the image file
        $imgnewname = md5($bookimg . time()) . $extension;
        // Code for move image into directory

        if (!in_array($extension, $allowed_extensions)) {
            echo "<script>alert('Invalid format. Only jpg / jpeg/ png /gif format allowed');</script>";
        } else {
            move_uploaded_file($_FILES["bookpic"]["tmp_name"], "bookimg/" . $image);
            $sql = "INSERT INTO  book(image,book_name,pages,year,author_id,category_id) VALUES(:image,:book_name,:pages,:year,:author,:category_name)";
            $query = $conn->prepare($sql);
            $query->bindParam(':image', $image, PDO::PARAM_STR);
            $query->bindParam(':book_name', $book_name, PDO::PARAM_STR);
            $query->bindParam(':pages', $pages, PDO::PARAM_STR);
            $query->bindParam(':year', $year, PDO::PARAM_STR);
            $query->bindParam(':author', $author_name, PDO::PARAM_STR);
            $query->bindParam(':category_name', $category_name, PDO::PARAM_STR);



            $query->execute();
            $lastInsertId = $conn->lastInsertId();
            if ($lastInsertId) {
                echo "<script>alert('Book Listed successfully');</script>";
                echo "<script>window.location.href='manage-books.php'</script>";
            } else {
                echo "<script>alert('Something went wrong. Please try again');</script>";
                echo "<script>window.location.href='manage-books.php'</script>";
            }
        }
    }
?>
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Online Library Management System | Add Book</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="../proekt2//admin/assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONT AWESOME STYLE  -->
        <link href="../proekt2//admin/assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE  -->
        <link href="../proekt2//admin/assets/css/style.css" rel="stylesheet" />

        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script type="text/javascript">
            function checkisbnAvailability() {
                $("#loaderIcon").show();
                jQuery.ajax({
                    url: "check_availability.php",
                    data: 'isbn=' + $("#isbn").val(),
                    type: "POST",
                    success: function(data) {
                        $("#isbn-availability-status").html(data);
                        $("#loaderIcon").hide();
                    },
                    error: function() {}
                });
            }
        </script>
    </head>

    <body>
        <!------MENU SECTION START-->
        <?php include('includes/header.php'); ?>
        <!-- MENU SECTION END-->
        <div class="content-wrapper">
            <div class="container">
                <div class="row pad-botm">
                    <div class="col-md-12">
                        <h4 class="header-line">Add Book</h4>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Book Info
                            </div>
                            <div class="panel-body">
                                <form role="form" method="post" enctype="multipart/form-data">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Book Name<span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="book_name" autocomplete="off" required />
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label> Category<span style="color:red;">*</span></label>
                                            <select class="form-control" name="category_name" required="required">
                                                <option value=""> Select Category</option>



                                                <?php

                                                $sql = "SELECT * from  category";
                                                $query = $conn->prepare($sql);
                                                $query->execute();
                                                $results = $query->fetchAll(PDO::FETCH_OBJ);
                                                $cnt = 1;
                                                if ($query->rowCount() > 0) {
                                                    foreach ($results as $result) {               ?>
                                                        <option value="<?php echo htmlentities($result->id); ?>"><?php echo htmlentities($result->category_name); ?></option>
                                                <?php }
                                                } ?>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label> Author<span style="color:red;">*</span></label>
                                            <select class="form-control" name="author_name" required="required">

                                                <option value=""> Select Author Name</option>

                                        </div>
                                    </div>


                                    <?php

                                    $sql = "SELECT * from  author";
                                    $query = $conn->prepare($sql);
                                    $query->execute();
                                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt = 1;
                                    if ($query->rowCount() > 0) {
                                        foreach ($results as $result) {               ?>
                                            <option value="<?php echo htmlentities($result->id); ?>"><?php echo htmlentities($result->author_name); ?></option>
                                    <?php }
                                    } ?>
                                    </select>
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Price<span style="color:red;">*</span></label>
                                <input class="form-control" type="text" name="year" autocomplete="off" required="required" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Book Picture<span style="color:red;">*</span></label>
                                <input class="form-control" type="file" name="bookpic" autocomplete="off" required="required" />
                            </div>
                        </div>
                        <button type="submit" name="add" id="add" class="btn btn-info">Submit </button>
                    </div>
                </div>
            </div>

        </div>

        </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <?php include('includes/footer.php'); ?>
        <!-- FOOTER SECTION END-->
        <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY  -->
        <script src="../proekt2/admin/assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="../proekt2/admin/assets/js/bootstrap.js"></script>
        <!-- CUSTOM SCRIPTS  -->
        <script src="../proekt2/admin/assets/js/custom.js"></script>
    </body>

    </html>
<?php } ?>